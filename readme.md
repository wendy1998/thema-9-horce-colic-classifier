# Horse colic classifier

This project is about building a java wrapper for a machine learning model 
that classifies whether a horse would survive the disease colic or not.   
This project can be used to classify new instances for the model that 
was used in the machine learning project.

# Motivation
This project was made to make the machine learning classification model 
spoken of in the previous paragraph easier to access.

# Build status
Finished

# Screenshots
### Processing and Classification
![Processing & Classification](screenshots/processing_classification.png)

### Results
#### whole
![Results whole](screenshots/results.png)

#### only classlabel
![Results classlabel](screenshots/results_cls.png)

# Technology used
- Weka for Java

### Built With
- IntelliJ IDEA

# Features
This program can classify instances from files and solitary instances from the commandline.
It can accept csv and arff files and can put the results out in the following way:
- to one output file which has an arff header and beneath the @data line, 
the output will be written underneath an identifying header for every file and if present,
the solitary instances.
- to several output files: one file for every set of instances to be classified. 
Again with an arff header above it.
- to the commandline: same as with one file, only now written to the commandline.

The results can be written as the original input instances + classlabel, 
or only as classlabel.

It can remove redundant attributes from the input files, 
so that only the needed attributes remain and this will be written to the files with filenames
[directories/]clean_[original_filenames].[original_extentions]   
It can also accept solitary unknown instances from the commandline and classify those.
If multiple files are specified for output, then these outputs will be written to the first file.

# Code example
```Java
// load ML model
CostSensitiveClassifier model = loadClassifier();

// classify instances
// classifying solitary unknown instances if needed
if(this.unknownInstances != null) {
    System.out.println("    Classifying solitary instances...");
    this.unknownInstances = addClass(this.unknownInstances);
    this.unknownInstances.setClassIndex(5);
    System.out.println("    solitary instances:");
    printInstances(this.unknownInstances);
    this.classifiedInstances.add(classifyNewInstance(model, this.unknownInstances));
}

// classify instances from files if needed
if(this.unknownFiles != null) {
    for (String fileName : this.unknownFiles) {
        System.out.println("    Classifying instances from file " + fileName);
        Instances unknownInstancesFromFile = loadInstancesFromFile(fileName);
        printInstances(unknownInstancesFromFile);
        this.classifiedInstances.add(classifyNewInstance(model, unknownInstancesFromFile));
    }
}
``` 

# Usage
```
usage: horseColicClassifier
 -h,--help                              Prints this message
 -of,--outputfiles <arg>                Either one of the following
                                        things:
                                        - one output file for all the data
                                        to be written to
                                        - one output file for every input
                                        file and one for the solitary
                                        unknown instances if provided.
                                        The file for the solitary
                                        instances needs to be at the
                                        beginning of the list.
                                        When not provided, then output
                                        will be written to the
                                        commandline.
                                        When output file already exist,
                                        the output for the corresponding
                                        data will be written to the
                                        commandline with an identifiable
                                        header above it.
 -ot,--outputtype <arg>                 The output type (whole or only
                                        classlabel), defaults to whole.
 -tef,--unknowndatafiles <arg>          The file(s) with the unknown data
                                        (csv or arff).
                                        Every input file will be cleaned
                                        of unnecessary attributes and the
                                        file [directories/]clean_[original
                                        filename] will be created for
                                        every file, even if this file
                                        already exists.
                                        the following attributes need to
                                        be present in the file:
                                        rec.temp numeric
                                        mucous {'normal pink','bright
                                        pink','pale pink','pale
                                        cyanotic','bright red/
                                        injected','dark cyanotic'}
                                        pain {'alert, no
                                        pain',depressed,'intermittent mild
                                        pain','intermittend severe
                                        pain','continuous severe pain'}
                                        abd.distention {none, slight,
                                        moderate, severe}
                                        gas.tube {none, slight,
                                        significant}
                                        Every value will be converted to
                                        lower case.
 -ui,--solitaryunknowninstances <arg>   Solitary unknown instances to be
                                        classified, fields separated by a
                                        ";", only the following attributes
                                        (in that order):
                                        rec.temp numeric
                                        mucous {'normal pink','bright
                                        pink','pale pink','pale
                                        cyanotic','bright red/
                                        injected','dark cyanotic'}
                                        pain {'alert, no
                                        pain',depressed,'intermittent mild
                                        pain','intermittend severe
                                        pain','continuous severe pain'}
                                        abd.distention {none, slight,
                                        moderate, severe}
                                        gas.tube {none, slight,
                                        significant}
                                        Every value will be converted to
                                        lower case.
```

# License
Licensed under GPLv3. See gpl.md
