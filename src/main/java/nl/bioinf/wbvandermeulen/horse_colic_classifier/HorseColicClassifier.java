/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */
package nl.bioinf.wbvandermeulen.horse_colic_classifier;

// package imports
import nl.bioinf.wbvandermeulen.horse_colic_classifier.commandline.*;
import nl.bioinf.wbvandermeulen.horse_colic_classifier.data_processing.*;
import nl.bioinf.wbvandermeulen.horse_colic_classifier.output_writing.*;
import nl.bioinf.wbvandermeulen.horse_colic_classifier.wekaintegration.WekaRunner;

// weka imports
import weka.core.Instance;
import weka.core.Instances;

// java imports
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Basic runner class to run the entire program
 */
public class HorseColicClassifier {
    private ApacheCliOptionsProvider optionsProvider;
    private List<String> unknownFiles;
    private String outputType;
    private String[] unknownInstances;
    private List<String> outputFiles;

    public static void main(String[] args) {
        HorseColicClassifier hcc = new HorseColicClassifier();
        hcc.start(args);
    }

    /**
     * Runs the entire program
     * @param args the commandline arguments
     */
    private void start(String[] args) {
        parseCommandline(args);
        if(optionsProvider.helpRequested()) {
            return;
        }

        // file processing
        ArrayList<String> cleanFiles = new ArrayList<>();
        if(unknownFiles != null) {
            cleanFiles = processFiles();
        }

        // solitary instance processing
        Instances solitaryInstances = null;
        if(unknownInstances != null) {
            solitaryInstances = processSolitaryInstances();
        }

        // classification
        WekaRunner runner = classifyInstances(solitaryInstances, cleanFiles);

        // results
        printResult(runner);
    }

    /**
     * Parses the commandline arguments
     * @param args the commandline arguments
     */
    private void parseCommandline(String[] args) {
        try {
            this.optionsProvider = new ApacheCliOptionsProvider(args);
            if (optionsProvider.helpRequested()) {
                optionsProvider.printHelp();
                return;
            }
            optionsProvider.checkArguments();

            // save args
            this.unknownFiles = optionsProvider.getUnknownFiles();
            this.outputType = optionsProvider.getOutputType();
            this.unknownInstances = optionsProvider.getUnknownInstances();
            this.outputFiles = optionsProvider.getOutputFiles();

        } catch (Exception ex) {
            System.out.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.out.println("Parsing failed. " + ex.getMessage());
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(new String[]{});
            op.printHelp();
            System.exit(0);
        }
    }

    /**
     * Process files with data to be classified
     * @return processed filenames
     */
    private ArrayList<String> processFiles() {
        int numIncorrectFiles = 0;
        ArrayList<String> cleanFiles = new ArrayList<>();
        System.out.println("\nFile processing...");
        for(String unknownFile : this.unknownFiles) {
            String fileName = unknownFile.substring(unknownFile.lastIndexOf("/")+1, unknownFile.indexOf("."));
            /*process file*/
            System.out.println("    Processing file " + unknownFile);
            if(unknownFile.endsWith(".csv")) {
                ProcessCSV csvReader = new ProcessCSV(unknownFile, "testdata/clean_" + fileName + ".csv");
                ArrayList<String> processedLines = csvReader.processFile();
                if(processedLines.size() > 1) {
                    csvReader.writeDestFile(processedLines);
                    cleanFiles.add("testdata/clean_" + fileName + ".csv");
                } else {
                    numIncorrectFiles++;
                    System.out.println("No instances left in file " + fileName + " to classify, because all the instances are faulty.");
                }
            } else {
                ProcessArff arffReader = new ProcessArff(unknownFile, "testdata/clean_" + fileName + ".arff");
                ArrayList<String> processedLines = arffReader.processFile();
                if(processedLines.size() > 0) {
                    if (arffReader.allGood) {
                        arffReader.writeDestFile(processedLines);
                    } else {
                        arffReader.writeDestFile(processedLines, arffReader.getHeaderData());
                    }
                    cleanFiles.add("testdata/clean_" + fileName + ".arff");
                } else {
                    numIncorrectFiles++;
                    System.out.println("No instances left in file " + fileName + " to classify, because all the instances are faulty.");
                }
            }
        }

        // no good files left
        if(numIncorrectFiles == unknownFiles.size()) {
            unknownFiles = null;
        }

        return cleanFiles;
    }

    /**
     * Processes the solitary instances given on the commandline
     * @return Instances, the solitary instances that got through the tests and need to be classified.
     */
    private Instances processSolitaryInstances() {
        System.out.println("\nProcess solitary instances...");
        Instances solitaryInstances;
        int numIncorrectInstances = 0;
        // create new instances object for the solitary instances
        solitaryInstances = new Instances("solitaryInstances", WantedAttributes.getAttributeList(),
                this.unknownInstances.length);
        // add individual instances to Instances object
        for(String instanceString: this.unknownInstances) {
            Instance instance = new ProcessUnknownInstance(instanceString.split(";")).getInstance();
            if(instance != null) {
                solitaryInstances.add(instance);
            } else {
                numIncorrectInstances += 1;
            }
        }

        System.out.println("Number of instances that won't be classified: " + numIncorrectInstances);
        // if the are (multiple) output files and no more solitary instances to be classified,
        // remove output file for solitary instances
        if((this.outputFiles != null && this.outputFiles.size() > 1) && solitaryInstances.numInstances() == 0) {
            this.outputFiles.remove(0);
        }

        if(solitaryInstances.numInstances() == 0) {
            this.unknownInstances = null;
        }

        return solitaryInstances;
    }

    /**
     * Classifies all the instances that need to be classified
     * @param solitaryInstances The instances that were given solitary to the commandline
     * @param cleanFiles The filenames of the files in which instances are to be classified.
     * @return the wekarunner that has done the classification, from which the results can be obtained
     */
    private WekaRunner classifyInstances(Instances solitaryInstances, ArrayList<String> cleanFiles) {
        System.out.println("\nClassifying new instances...");
        WekaRunner runner;
        if(unknownFiles == null && unknownInstances == null) {
            throw new IllegalStateException("No instances to classify, they have all been denied.");
        } else {
            runner = new WekaRunner(solitaryInstances, cleanFiles);
        }

        return runner;
    }

    /**
     * Prints the results to the commandline or 1 or more files
     * @param runner The wekarunner from which the results can be obtained.
     */
    private void printResult(WekaRunner runner) {
        System.out.println("\nResults classification (output method: " + outputType + "):");
        ArrayList<Instances> classifiedInstances = runner.getClassifiedInstances();
        ArrayList<String> headerData = makeHeaderData(unknownInstances != null, unknownFiles);
        if(headerData.size() == 1) {
            headerData = null;
        }

        // to file
        if(outputFiles != null) {
            OutputWriterFiles fileWriter = new OutputWriterFiles(outputType, outputFiles);
            if(outputFiles.size() > 1) {
                fileWriter.printOutputMultipleFiles(classifiedInstances, headerData);
            } else {
                fileWriter.printOutputSingleFile(classifiedInstances, headerData, outputFiles.get(0), Boolean.TRUE);
            }
            // to commandline
        } else {
            OutputWriterCommandline commandlinePrinter = new OutputWriterCommandline(outputType);
            commandlinePrinter.printOutputCommandlineAllInstances(classifiedInstances, headerData);
        }
    }

    /**
     * Makes unique headers for every Instances object in the output. Used to identify different sets of instances.
     * @param unknownInstances Whether there are solitary unknown instances in the results or not
     * @param unknownFiles The filenames of the files in which instances stood before the classification
     * @return A list with strings to identify the different components of the header by.
     */
    private ArrayList<String> makeHeaderData(boolean unknownInstances, List<String> unknownFiles) {
        ArrayList<String> headerData = new ArrayList<>();

        if(unknownInstances) {
            headerData.add("solitary instances:");
        }

        if(unknownFiles != null) {
            for(String fileName: unknownFiles) {
                headerData.add("\ninstances from file " + fileName + ":");
            }
        }
        return headerData;
    }
}
