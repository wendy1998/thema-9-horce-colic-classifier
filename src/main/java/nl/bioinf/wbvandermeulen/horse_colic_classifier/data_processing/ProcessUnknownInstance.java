/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */
package nl.bioinf.wbvandermeulen.horse_colic_classifier.data_processing;

import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;

import java.util.ArrayList;

/**
 * class to process solitary unknown instances from the commandline
 */
public class ProcessUnknownInstance {
    private static final ArrayList<Attribute> attributeOptions = new ArrayList<>(5);
    private Instance instance = new DenseInstance(5);
    private boolean success = Boolean.TRUE;

    public static void main(String[] args) {
        createAttributes();
    }

    public ProcessUnknownInstance(String[] values) {
        createAttributes();
        instantiate(values);
    }

    /**
     * Processes the entire instance
     * @param values the values that the attributes need to have
     */
    private void instantiate(String[] values) {
        try {
            checkInstance(values);
        } catch(Exception e) {
            System.out.println("Error: " + e.getMessage() + "\nthis instance won't be classified.");
            this.success = Boolean.FALSE;
            return;
        }
        createInstance(values);
    }

    /**
     * Checks whether the values of the instance are allowed and if there are enough values.
     * @param values the values that the attributes need to have.
     */
    private void checkInstance(String[] values) {
        // check the amount of values given
        if(values.length > 5) {
            throw new IllegalStateException("you gave too many attributes. Wanted attributes:\n" + WantedAttributes.printAttributes());
        } else if(values.length < 5) {
            throw new IllegalStateException("you didn't give enough attributes. Wanted attributes:\n" + WantedAttributes.printAttributes());
        }

        // check whether rectal temperature got a numeric value
        try {
            if(!values[0].equals("?")) {
                Double.parseDouble(values[0]);
            }
        } catch (NumberFormatException e) {
            throw new IllegalStateException("the value for rectal temperature isn't numeric.");
        }

        // check values for nominal attributes
        for(int i = 1; i < 5; i++) {
            WantedAttributes attr = WantedAttributes.values()[i];
            String value = values[i].replace("\'", "").toLowerCase();
            if(!(attr.getOptions().contains(value) || value.equals("?"))) {
                throw new IllegalArgumentException("Wrong value for attribute " + attr.getAttributeName() + "." +
                        "\nyour value: " + values[i] + "." +
                        "\npossible values:\n" + String.join("\n - ", attr.getOptions()));
            }
        }
    }

    /**
     * creates an instance object from the given values
     * @param values the values that the attributes need to have.
     */
    private void createInstance(String[] values) {
        for(int i=0; i < 5; i++) {
            Attribute attr = attributeOptions.get(i);
            if(values[i].equals("?")) {
                if(attr.isNumeric()) {
                    instance.setValue(attr, 8);
                } else {
                    instance.setValue(attr, WantedAttributes.values()[i].getOptions().get(0));
                }
                instance.setMissing(attr);
            } else if(attr.isNumeric()) {
                instance.setValue(attr, Double.parseDouble(values[i]));
            } else {
                String value = values[i].replace("\'", "").toLowerCase();
                instance.setValue(attr, value);
            }
        }
    }

    /**
     * Creates attribute objects that are needed for the creation of the instance objects.
     */
    private static void createAttributes() {
        for(int i=0; i < 5; i++) {
            Attribute attr = WantedAttributes.values()[i].getAttributeObject();
            attributeOptions.add(attr);
        }
    }

    /**
     * serves the created instance object, if creation was a success.
     * @return created instance object
     */
    public Instance getInstance() {
        if(this.success) {
            return this.instance;
        } else {
            return null;
        }
    }
}
