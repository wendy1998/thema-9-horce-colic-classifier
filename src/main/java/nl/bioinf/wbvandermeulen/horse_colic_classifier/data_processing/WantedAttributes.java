/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */
package nl.bioinf.wbvandermeulen.horse_colic_classifier.data_processing;

import weka.core.Attribute;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Enum that represents the attributes wanted for the classification.
 */
public enum WantedAttributes {
    REC_TEMP("rec.temp", new String[] {"not applicable"}),
    MUCOUS("mucous", new String[] {"normal pink","bright pink","pale pink",
            "pale cyanotic","bright red/ injected","dark cyanotic"}),
    PAIN("pain", new String[] {"alert, no pain", "depressed", "intermittent mild pain",
            "intermittend severe pain", "continuous severe pain"}),
    ABD_DISTENTION("abd.distention", new String[] {"none", "slight", "moderate", "severe"}),
    GAS_TUBE("gas.tube", new String[] {"none", "slight", "significant"});

    private String attribute;
    private String[] options;
    private Attribute attributeObject;
    private static ArrayList<Attribute> attributeList = createAttributeList();

    WantedAttributes(String attribute, String[] options ) {
        this.attribute = attribute;
        this.options = options;
        createAttributeObject();
    }

    /**
     * Create a list with the wanted attributes as Attribute class objects
     * @return attributeList the list with attribute objects
     */
    private static ArrayList<Attribute> createAttributeList() {
        ArrayList<Attribute> AttributeList = new ArrayList<>();
        for(WantedAttributes attr:WantedAttributes.values()) {
            AttributeList.add(attr.getAttributeObject());
        }
        return AttributeList;
    }

    /**
     * create an object off class Attribute from the text representations of the wanted attributes.
     */
    private void createAttributeObject() {
        List<String> options = getOptions();
        // numeric attribute
        if(options.get(0).equals("not applicable")) {
            this.attributeObject = new Attribute(this.getAttributeName());
        } else {
            this.attributeObject =  new Attribute(this.getAttributeName(), options);
        }
    }

    /**
     * Prints all the wanted attributes and their possible values.
     * @return the string representation of the attributes and their possible values
     */
    public static String printAttributes() {
        return "rec.temp numeric\nmucous {'normal pink','bright pink','pale pink','pale cyanotic','bright red/ injected','dark cyanotic'}" +
                "\npain {'alert, no pain',depressed,'intermittent mild pain','intermittend severe pain','continuous severe pain'}" +
                "\nabd.distention {none, slight, moderate, severe}\ngas.tube {none, slight, significant}";
    }

    // getters
    public static ArrayList<Attribute> getAttributeList() {
        return attributeList;
    }

    public String getAttributeName() {
        return attribute;
    }

    public List<String> getOptions() {
        return Arrays.asList(options);
    }

    public Attribute getAttributeObject() {
        return this.attributeObject;
    }
}
