/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */
package nl.bioinf.wbvandermeulen.horse_colic_classifier.data_processing;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class to process a CSV file, subclass of FileProcessor
 */
public class ProcessCSV extends FileProcessor {
    public ProcessCSV(String fileName, String destFile) {
        this.fileName = fileName;
        this.destFile = destFile;
        this.painIndex = 2;
    }

    /**
     * Finds on which indices the needed attributes lay when converting instance to a list.
     * @param lines the lines from the input file
     * @return indices on which the needed attributes lay when converting instance to a list.
     */
    @Override
    int[] findAttributeIndices(ArrayList<String> lines) {
        String header = lines.get(0);
        String[] headerSplit = header.split(",");
        this.headerLength = headerSplit.length;
        int[] indices = new int[5];
        for(int i = 0; i < WantedAttributes.values().length; i++) {
            String attribute = WantedAttributes.values()[i].getAttributeName();
            int index = Arrays.asList(headerSplit).indexOf(attribute);
            if(index < 0) {
                System.out.println("Error: not all the needed attributes are present.\n" +
                        "Needed attributes:\n");
                for(WantedAttributes attr: WantedAttributes.values()) {
                    System.out.println("- " + attr.getAttributeName());
                }
                System.exit(0);
            }
            indices[i] = index;
        }
        if(headerLength == 5) {
            allGood = Boolean.TRUE;
        }
        return indices;
    }
}
