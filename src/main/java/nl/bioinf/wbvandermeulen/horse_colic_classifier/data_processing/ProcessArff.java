/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */
package nl.bioinf.wbvandermeulen.horse_colic_classifier.data_processing;

import java.util.ArrayList;

/**
 * class to process an arff file, subclass of FileProcessor
 */
public class ProcessArff extends FileProcessor {
    private ArrayList<String> headerData = new ArrayList<>();
    public ProcessArff(String fileName, String destFile) {
        this.fileName = fileName;
        this.destFile = destFile;
        this.isArff = Boolean.TRUE;
    }

    /**
     * Find the lines with the instances only
     * @param lines the lines from the input file
     * @return lines with only instances
     */
    static ArrayList<String> ParseDataLines(ArrayList<String> lines) {
        ArrayList<String> dataLines = new ArrayList<>();
        int flag = 0;

        for(String line: lines) {
            if(flag == 1) {
                dataLines.add(line);
            } else if(line.split(" ")[0].equals("@data")) {
                flag = 1;
            }
        }

        return dataLines;
    }

    /**
     * Finds on which indices the needed attributes lay when converting instance to a list.
     * @param lines the lines from the input file
     * @return indices on which the needed attributes lay when converting instance to a list.
     */
    @Override
    public int[] findAttributeIndices(ArrayList<String> lines) {
        int[] indices = new int[5];
        /*to keep track of which index in the array needs to be filled*/
        int indicesIndex = 0;
        /*to keep track of the indices of the attributes*/
        int attributeIndex = 0;

        for(String line: lines) {
            if(line.startsWith("@data")) {
                this.headerData.add(line);
                break;
            }
            if(line.startsWith("@attribute")) {
                String attribute = line.split(" ")[1];
                for(WantedAttributes attr: WantedAttributes.values()) {
                    if(!(attribute.equals("outcome")) && attribute.equals(attr.getAttributeName())) {
                        indices[indicesIndex] = attributeIndex;
                        if(attr.getAttributeName().equals("pain")) {
                            this.painIndex = indicesIndex;
                        }
                        indicesIndex += 1;
                        this.headerData.add(line);
                    }
                }
                attributeIndex += 1;
            } else {
                this.headerData.add(line);
            }
        }

        /*check whether all needed attributes are present*/
        if(indices[indices.length-1] == 0) {
            System.out.println("Error: not all the needed attributes are present.\n" +
                    "Needed attributes:\n");
            for(WantedAttributes attr: WantedAttributes.values()) {
                System.out.println("- " + attr.getAttributeName());
            }
            System.exit(0);
        }

        if(attributeIndex == 5) {
            this.allGood = Boolean.TRUE;
        }
        this.headerLength = attributeIndex;
        return indices;
    }

    /**
     * returns the header of the new file
     * @return the header for the clean file
     */
    public ArrayList<String> getHeaderData() {
        return headerData;
    }
}
