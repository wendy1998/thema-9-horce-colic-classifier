/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */
package nl.bioinf.wbvandermeulen.horse_colic_classifier.data_processing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

/**
 * abstract class for processing files
 */
public abstract class FileProcessor {
    String fileName;
    String destFile;
    public boolean allGood = Boolean.FALSE;
    boolean isArff = Boolean.FALSE;
    int headerLength;
    int painIndex;

    /**
     * Reads every line of the input file and saves it
     * @return lines from the input file.
     */
    private ArrayList<String> readFile() {
        Path path = Paths.get(fileName);
        ArrayList<String> lines = new ArrayList<>();
        try (BufferedReader reader = Files.newBufferedReader(path, Charset.defaultCharset())){
            String lineFromFile;
            while((lineFromFile = reader.readLine()) != null) {
                lines.add(lineFromFile.toLowerCase());
            }
        } catch(IOException e) {
            System.out.println("Error while reading file.");
            System.exit(0);
        }
        return lines;
    }

    /**
     * Processes the entire file, making use of other methods and subclasses.
     * @return processedLines, the lines of the instances with only the needed attributes,
     * in lower case, without quotes, with allowed attribute values.
     */
    public ArrayList<String> processFile() {
        // read the datafile
        ArrayList<String> lines = readFile();
        ArrayList<String> processedLines = new ArrayList<>();

        // find on which indices the wanted attributes are when you make a list of the line
        int[] indices = findAttributeIndices(lines);
        // index on which the attribute pain lays, because of a problem with the separator
        int tipPoint = indices[painIndex];

        // header is exactly the attributes needed, nothing more, nothing less
        if(allGood) {
            return lines;
        }

        // if arff file, then not all the lines are data
        if(isArff) {
            lines = ProcessArff.ParseDataLines(lines);
        }

        // cleaning of the instances
        String[] splitLine;
        int numWrongLines = 0;
        for(int i = 0; i < lines.size(); i++) {
            splitLine = lines.get(i).split(",");
            boolean up = Boolean.FALSE;
            // problematic value in the pain attribute (with a "," in it)
            if(splitLine.length > headerLength) {
                // put pain value back together and add one to all indices above the pain attribute while processing line
                splitLine[indices[painIndex]] = splitLine[indices[painIndex]] + "," + splitLine[indices[painIndex] + 1];
                up = Boolean.TRUE;
            }
            ArrayList<String> processedLine = new ArrayList<>();
            for(int idx=0; idx < indices.length; idx++) {
                if(up) {
                    if(indices[idx] <= tipPoint) {
                        processedLine.add(splitLine[indices[idx]]);
                    } else {
                        processedLine.add(splitLine[indices[idx] + 1]);
                    }
                } else {
                    processedLine.add(splitLine[indices[idx]]);
                }
            }
            if(i == 0 | checkAttributeValues(processedLine)) {
                processedLines.add(String.join(",", processedLine));
            } else {
                numWrongLines++;
                System.out.println(numWrongLines + " line(s) found with faulty attribute values. Options per attribute:\n" +
                        WantedAttributes.printAttributes());
            }
        }
        return processedLines;
    }

    /**
     * Checks whether all the values for the needed attributes are allowed for those attributes.
     * @param instance The instance to be checked, a list with string values.
     * @return whether the instance passed the test.
     */
    private boolean checkAttributeValues(ArrayList<String> instance) {
        boolean passed = Boolean.TRUE;
        if(!isArff) {
            for (int i = 1; i < instance.size(); i++) {
                WantedAttributes attr = WantedAttributes.values()[i];
                if (!(instance.get(i).equals("?") | attr.getOptions().contains(instance.get(i).toLowerCase().replace("\'",
                        "")))) {
                    passed = Boolean.FALSE;
                }
            }
        } else {
            for (int i = 0; i < instance.size(); i++) {
                WantedAttributes attr = WantedAttributes.values()[i];
                if (!(instance.get(i).equals("?") | attr.getOptions().contains(instance.get(i).toLowerCase().replace("\'",
                        "")))) {
                    passed = Boolean.FALSE;
                }
            }
        }

        return passed;
    }

    /**
     * Find the indices on which the attributes needed lay, when instance is converted to a list of values.
     * @param lines the lines from the input file
     * @return an intArray with 5 values: for every needed attribute the index on which it lays in a list.
     */
    abstract int[] findAttributeIndices(ArrayList<String> lines);

    /**
     * Write file in which only the cleaned/approved data is present.
     * @param processedLines The lines that need to be written to the file.
     */
    public void writeDestFile(ArrayList<String> processedLines) {
        writeDestFile(processedLines, null);
    }

    /**
     * Write file in which only the cleaned/approved data is present.
     * @param processedLines The lines that need to be written to the file
     * @param headerData The header for above the data (in the case of arff files)
     */
    public void writeDestFile(ArrayList<String> processedLines, ArrayList<String> headerData) {
        Path path = Paths.get(destFile);
        try {
            Files.deleteIfExists(path);
        } catch(IOException e) {
            System.out.println("Error while writing new file, exiting now...");
            System.exit(0);
        }

        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(path, Charset.defaultCharset(),
                StandardOpenOption.CREATE))) {
            if(headerData != null) {
                for (String line : headerData) {
                    writer.println(line);
                }
            }

            for(String line:processedLines) {
                writer.println(line);
            }
        } catch(IOException e) {
            System.out.println("Error while writing new file, exiting now...");
            System.exit(0);
        }
    }
}
