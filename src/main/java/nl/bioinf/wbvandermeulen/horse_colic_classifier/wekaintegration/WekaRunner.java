/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */
package nl.bioinf.wbvandermeulen.horse_colic_classifier.wekaintegration;

// weka imports
import weka.classifiers.meta.CostSensitiveClassifier;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;

// java imports
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Main class for classification. uses weka packages to classify unknown instances.
 */
public class WekaRunner {
    private final ArrayList<String> unknownFiles;
    private Instances unknownInstances;
    private ArrayList<Instances> classifiedInstances = new ArrayList<>();

    public WekaRunner(final Instances solitaryInstances, final ArrayList<String> unknownFiles) {
        this.unknownFiles = unknownFiles;
        this.unknownInstances = solitaryInstances;
        start();
    }

    /**
     * Does the entire classification procedure
     */
    private void start() {
        try {
            // load ML model
            CostSensitiveClassifier model = loadClassifier();

            // classify instances
            // classifying solitary unknown instances if needed
            if(this.unknownInstances != null) {
                System.out.println("    Classifying solitary instances...");
                this.unknownInstances = addClass(this.unknownInstances);
                this.unknownInstances.setClassIndex(5);
                System.out.println("    solitary instances:");
                printInstances(this.unknownInstances);
                this.classifiedInstances.add(classifyNewInstance(model, this.unknownInstances));
            }

            // classify instances from files if needed
            if(this.unknownFiles != null) {
                for (String fileName : this.unknownFiles) {
                    System.out.println("    Classifying instances from file " + fileName);
                    Instances unknownInstancesFromFile = loadInstancesFromFile(fileName);
                    printInstances(unknownInstancesFromFile);
                    this.classifiedInstances.add(classifyNewInstance(model, unknownInstancesFromFile));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Predicts the class label for the given instances using the model that was given to it too.
     * @param model the ML model
     * @param unknownInstances The instances to be classified
     * @return labeled The classified and labeled instances.
     * @throws Exception when an instance can't be labeled.
     */
    private Instances classifyNewInstance(CostSensitiveClassifier model,
                                     Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = model.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        return labeled;
    }

    /**
     * loads the machine learning model from file
     * @return the machine learning model
     * @throws Exception if model can't be loaded
     */
    private CostSensitiveClassifier loadClassifier() throws Exception {
        // deserialize model
        String modelFile = "testdata/NB_model.model";
        return (CostSensitiveClassifier) weka.core.SerializationHelper.read(modelFile);
    }

    /**
     * Prints a selection of the instances given to it to the commandline
     * @param instances the instances to be printed
     */
    private void printInstances(Instances instances) {
        int numAttributes = instances.numAttributes();

        for (int i = 0; i < numAttributes; i++) {
            System.out.println("attribute " + i + " = " + instances.attribute(i));
        }

        System.out.println("class index = " + instances.classIndex());
        int numInstances = instances.numInstances();
        for (int i = 0; i < numInstances; i++) {
            if (i == 5) break;
            Instance instance = instances.instance(i);
            System.out.println("instance = " + instance);
        }
    }

    /**
     * Adds the class attribute to the instances given to it
     * @param data instances without class attribute
     * @return newData instances with class attribute
     */
    private Instances addClass(Instances data) {
        Instances newData = new Instances(data);
        Add filter = new Add();
        filter.setAttributeIndex("last");
        filter.setNominalLabels("lived, died");
        filter.setAttributeName("outcome");
        try {
            filter.setInputFormat(newData);
            newData = Filter.useFilter(newData, filter);
        } catch(Exception e) {
            System.out.println("Error: something went wrong while loading data.");
            System.exit(0);
        }
        return newData;
    }

    /**
     * Loads instances from a given datafile, if csv file, then file will first be converted to an arff file.
     * @param datafile the file from which the data must be loaded
     * @return loaded instances
     * @throws IOException if file given does not exist, or something else goes wrong during reading.
     */
    private Instances loadInstancesFromFile(String datafile) throws IOException {
        try {
            Instances data;
            // load instances with CSV loader
            if(datafile.endsWith(".csv")) {
                // load CSV
                CSVLoader loader = new CSVLoader();
                loader.setSource(new File(datafile));
                data = loader.getDataSet();
                data = addClass(data);
            } else {
                // load instances with DataSource class
                DataSource source = new DataSource(datafile);
                data = source.getDataSet();
                data = addClass(data);
            }
            data.setClassIndex(5);

            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }

    // getters
    public ArrayList<Instances> getClassifiedInstances() {
        return this.classifiedInstances;
    }

}