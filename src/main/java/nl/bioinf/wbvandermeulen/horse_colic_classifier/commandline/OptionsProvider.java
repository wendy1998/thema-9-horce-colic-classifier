/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */
package nl.bioinf.wbvandermeulen.horse_colic_classifier.commandline;

import java.util.List;

/**
 * interface that specifies which options should be provided to the tool.
 * @author Wendy
 */
public interface OptionsProvider {
    List<String> getUnknownFiles();

    String getOutputType();

    String[] getUnknownInstances();

    List<String> getOutputFiles();
}
