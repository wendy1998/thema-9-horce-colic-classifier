/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */
package nl.bioinf.wbvandermeulen.horse_colic_classifier.commandline;

import nl.bioinf.wbvandermeulen.horse_colic_classifier.data_processing.WantedAttributes;
import org.apache.commons.cli.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class implements the OptionsProvider interface by parsing the passed command line arguments.
 * @author wendy
 */
public class ApacheCliOptionsProvider implements OptionsProvider {
    private static final String UNKNOWN_FILES = "unknowndatafiles";
    private static final String UNKNOWN_INSTANCES = "solitaryunknowninstances";
    private static final String HELP = "help";
    private static final String OUTPUT_TYPE = "outputtype";
    private static final String OUTPUT_FILES = "outputfiles";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;
    private List<String> goodFiles = null;
    private List<String> outputFiles = null;

    /**
     * constructs with the command line array.
     * @param args the CL array
     */
    public ApacheCliOptionsProvider(final String[] args) {
        this.clArguments = args;
        this.initialize();
    }

    /**
     * Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * check if help was requested; if so, return true.
     * @return helpRequested
     */
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * check if right arguments are given.
     */
    public void checkArguments() {
        if(!(this.commandLine.hasOption(UNKNOWN_INSTANCES) | this.commandLine.hasOption(UNKNOWN_FILES))) {
            throw new IllegalStateException("Error: no instances to be classified provided.");
        }

        // needed for output files check after unknown files check
        boolean instances = Boolean.FALSE;
        boolean singleFile = Boolean.FALSE;
        if(this.commandLine.hasOption(OUTPUT_FILES)) {
            this.outputFiles = Arrays.asList(this.commandLine.getOptionValues(OUTPUT_FILES));
            if(this.outputFiles.size() == 1) {
                singleFile = Boolean.TRUE;
            }
            if(this.commandLine.hasOption(UNKNOWN_INSTANCES)) {
                instances = Boolean.TRUE;
            }
        }

        if(this.commandLine.hasOption(UNKNOWN_FILES)) {
            this.goodFiles = Arrays.asList(this.commandLine.getOptionValues(UNKNOWN_FILES));

            // check whether number of output files is right
            if(this.commandLine.hasOption(OUTPUT_FILES)) {
                try {
                    // need to add 1 to index when output file has to be removed, because individual instance file is before that
                    if(instances) {
                        // it's more than one output file
                        if (!singleFile && !(this.outputFiles.size() == this.goodFiles.size() + 1)) {
                            int size = this.outputFiles.size();
                            this.outputFiles = null;
                            throw new IllegalArgumentException("The right amount of output files wasn't given.\n" +
                                    "your amount: " + size + "\n" +
                                    "right amount: " + (this.goodFiles.size() + 1) + "\n" +
                                    "Output will be written to the commandline.");
                        }
                    } else {
                        if (!singleFile && !(this.outputFiles.size() == this.goodFiles.size())) {
                            int size = this.outputFiles.size();
                            this.outputFiles = null;
                            throw new IllegalArgumentException("The right amount of output files wasn't given.\n" +
                                    "your amount: " + size + "\n" +
                                    "right amount: " + this.goodFiles.size() + "\n" +
                                    "Output will be written to the commandline.");
                        }
                    }
                } catch(IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                }
            }

            // check whether unknown files exist and their file extensions
            // list with the indices of the output files to be removed after check.
            ArrayList<Integer> removeIndices = new ArrayList<>();
            for (int i = 0; i < this.goodFiles.size(); i++) {
                try {
                    String fileName = this.goodFiles.get(i);
                    // check whether files exist
                    Path path = Paths.get(fileName);
                    if (Files.notExists(path)) {
                        this.goodFiles.remove(fileName);
                        // output file for that input file needs to be removed too
                        if (this.outputFiles != null && !singleFile) {
                            if (instances) {
                                removeIndices.add(i+1);
                            } else {
                                removeIndices.add(i);
                            }
                        }
                        throw new IllegalArgumentException("Error: file " + fileName + " does not exist.\n" +
                                "This file won't be processed.");
                    }
                    // check file extension
                    if (!(fileName.endsWith(".csv") | fileName.endsWith(".arff"))) {
                        this.goodFiles.remove(fileName);
                        // output file for that input file needs to be removed too
                        if (this.outputFiles != null && this.outputFiles.size() > 1) {
                            if (instances) {
                                removeIndices.add(i+1);
                            } else {
                                removeIndices.add(i);
                            }
                        }
                        throw new IllegalArgumentException("Error: file " + fileName + " isn't an arff or csv file.\n" +
                                "This file won't be processed.");
                    }
                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                }
            }
            // remove the output files corresponding to the removed unknown files
            for(int idx: removeIndices) {
                this.outputFiles.remove(idx);
            }
        }

        // check output format
        if(this.commandLine.hasOption(OUTPUT_TYPE)) {
            if (!(this.commandLine.getOptionValue(OUTPUT_TYPE).toLowerCase().equals("whole") | this.commandLine.getOptionValue(OUTPUT_TYPE).toLowerCase().equals("classlabel"))) {
                throw new IllegalArgumentException("Error: output has to be one of (whole, classlabel), " +
                        "please provide new options.");
            }
        }
    }

    /**
     * builds the Options object.
     */
    private void buildOptions() {
        // create Options object
        this.options = new Options();

        // set options for the commandline
        Option helpOption = new Option("h", HELP, false, "Prints this message");
        Option unknownFilesOption = new Option("tef", UNKNOWN_FILES, true, "The file(s) with the unknown data (csv or arff).\n" +
                "Every input file will be cleaned of unnecessary attributes and the file " +
                "[directories/]clean_[original filename] will be created for every file, even if this file already exists.\n" +
                "the following attributes need to be present in the file:\n" + WantedAttributes.printAttributes() + "\n" +
                "Every value will be converted to lower case.");
        Option outputTypeOption = new Option("ot", OUTPUT_TYPE, true, "The output type (whole or only classlabel), defaults to whole.");
        Option unknownInstancesOption = new Option("ui", UNKNOWN_INSTANCES, true, "Solitary unknown instances to be classified, " +
                "fields separated by a \";\", only the following attributes (in that order):\n" + WantedAttributes.printAttributes() + "\n" +
                "Every value will be converted to lower case.");
        Option outputFileOption = new Option("of", OUTPUT_FILES, true, "Either one of the following things:\n" +
                " - one output file for all the data to be written to\n" +
                " - one output file for every input file and one for the solitary unknown instances if provided.\n" +
                "The file for the solitary instances needs to be at the beginning of the list.\n" +
                "When not provided, then output will be written to the commandline.\n" +
                "When output file already exist, the output for the corresponding data will be written to the commandline with " +
                "an identifiable header above it.");

        // these options can have more than 1 value
        unknownFilesOption.setArgs(Option.UNLIMITED_VALUES);
        unknownInstancesOption.setArgs(Option.UNLIMITED_VALUES);
        outputFileOption.setArgs(Option.UNLIMITED_VALUES);

        // add options to Options object.
        options.addOption(helpOption);
        options.addOption(unknownFilesOption);
        options.addOption(outputTypeOption);
        options.addOption(unknownInstancesOption);
        options.addOption(outputFileOption);
    }

    /**
     * processes the command line arguments.
     */
    private void processCommandLine() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);
        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex.getMessage());
        }
    }

    /**
     * prints help.
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("horseColicClassifier", options);
    }

    /**
     * gets the file with the data to be classified
     * @return unknownFile, the file with the data to be classified.
     */
    @Override
    public List<String> getUnknownFiles() {
        if(this.commandLine.hasOption(UNKNOWN_FILES)) {
            if (this.goodFiles.size() > 0) {
                return this.goodFiles;
            }
        }
       return null;
    }

    @Override
    public List<String> getOutputFiles() {
        if(this.commandLine.hasOption(OUTPUT_FILES)) {
            return this.outputFiles;
        }
        return null;
    }

    @Override
    public String[] getUnknownInstances() {
        if(this.commandLine.hasOption(UNKNOWN_INSTANCES)) {
            return this.commandLine.getOptionValues(UNKNOWN_INSTANCES);
        } else {
            return null;
        }
    }

    @Override
    public String getOutputType() {
        if(this.commandLine.hasOption(OUTPUT_TYPE)) {
            return this.commandLine.getOptionValue(OUTPUT_TYPE).toLowerCase();
        } else {
            return "whole";
        }
    }
}
