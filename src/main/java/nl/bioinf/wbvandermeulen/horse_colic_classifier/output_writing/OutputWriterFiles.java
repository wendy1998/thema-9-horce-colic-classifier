/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */
package nl.bioinf.wbvandermeulen.horse_colic_classifier.output_writing;

// weka imports
import weka.core.Instances;

// java imports
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to write classification output to files.
 */
public class OutputWriterFiles {
    private final String outputType;
    private final List<String> outputFiles;
    private boolean header = Boolean.FALSE;
    private OutputWriterCommandline commandlineWriter;

    public OutputWriterFiles(final String outputType, final List<String> outputFiles) {
        this.outputType = outputType;
        this.outputFiles = outputFiles;
        this.commandlineWriter = new OutputWriterCommandline(outputType);
    }

    /**
     * Prints the outcome of all the classifications to a single output file.
     * @param classifiedInstances the instances which were classified, including the predicted classlabel
     * @param stringHeaders for every instances object a header to identify it by in the output
     */
    public void printOutputSingleFile( ArrayList<Instances> classifiedInstances,
                                        ArrayList<String> stringHeaders, String outFile,
                                        boolean headerToFile) {
        boolean writeHeader = Boolean.TRUE;
        boolean caught = Boolean.FALSE;
        Path path = Paths.get(outFile);
        if(Files.notExists(path)) {
            try {
                Files.createFile(path);
            } catch (IOException e) {
                System.out.println(e.getMessage());
                System.out.println("Output will be written to the commandline");
                caught = Boolean.TRUE;
                if(!this.header) {
                    this.header = Boolean.TRUE;
                } else {
                    writeHeader = Boolean.FALSE;
                }
            }
        } else {
            System.out.println("Output file " + outFile + " already exists, output will be written to the commandline.");
            caught = Boolean.TRUE;
            if(!this.header) {
                this.header = Boolean.TRUE;
            } else {
                writeHeader = Boolean.FALSE;
            }
        }

        if(!caught) {
            try (BufferedWriter writer = Files.newBufferedWriter(path, Charset.defaultCharset(),
                    StandardOpenOption.APPEND)) {

                if (!this.outputType.equals("classlabel")) {
                    writeHeaderToFile(path);
                }

                for (int x = 0; x < classifiedInstances.size(); x++) {
                    Instances instancesNow = classifiedInstances.get(x);
                    if (stringHeaders != null && headerToFile) {
                        writer.write(stringHeaders.get(x) + "\n");
                    }
                    for (int i = 0; i < instancesNow.numInstances(); i++) {
                        if (this.outputType.equals("classlabel")) {
                            writer.write((i + 1) + ": " + instancesNow.get(i).toString(5) + "\n");
                        } else {
                            writer.write(instancesNow.get(i).toString() + "\n");
                        }
                    }
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        // print output to the commandline
        if(caught){
            if(writeHeader) {
                this.commandlineWriter.printOutputCommandlineAllInstances(classifiedInstances, stringHeaders);
            } else {
                this.commandlineWriter.printOutputCommandline(classifiedInstances.get(0), stringHeaders.get(0));
            }
        }
    }

    /**
     * Prints the output to multiple files
     * @param classifiedInstances The results that need to be printed
     * @param stringHeaders An identifyable header for every Instances object in classifiedInstances.
     */
    public void printOutputMultipleFiles(ArrayList<Instances> classifiedInstances, ArrayList<String> stringHeaders) {
        for(int i=0; i < classifiedInstances.size(); i++) {
            String outFile = this.outputFiles.get(i);
            Instances instancesNow = classifiedInstances.get(i);
            ArrayList<Instances> instancesWrapper = new ArrayList<>(1);
            instancesWrapper.add(instancesNow);
            ArrayList<String> headerWrapper = new ArrayList<>(1);
            if (stringHeaders != null) {
                headerWrapper.add(stringHeaders.get(i));
            } else {
                headerWrapper = null;
            }
            printOutputSingleFile(instancesWrapper, headerWrapper, outFile, Boolean.FALSE);
        }
    }

    /**
     * Write the arff header associated with the data of this model to a specific file
     * @param file the file to write the header to
     */
    private void writeHeaderToFile(Path file) {
        try (BufferedWriter writer = Files.newBufferedWriter(file, Charset.defaultCharset(),
                StandardOpenOption.APPEND)) {
            try (BufferedReader reader = Files.newBufferedReader(Paths.get("testdata/header.txt"),
                    Charset.defaultCharset())) {
                String lineFromFile;
                while ((lineFromFile = reader.readLine()) != null) {
                    writer.write(lineFromFile + "\n");
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}

