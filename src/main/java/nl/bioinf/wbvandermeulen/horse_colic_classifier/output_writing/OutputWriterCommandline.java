/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */
package nl.bioinf.wbvandermeulen.horse_colic_classifier.output_writing;

// weka imports
import weka.core.Instances;

// java imports
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class OutputWriterCommandline {
    private final String outputType;

    public OutputWriterCommandline(final String outputType) {
        this.outputType = outputType;
    }

    public void printOutputCommandlineAllInstances(ArrayList<Instances> classifiedInstances, ArrayList<String> stringHeader) {
        if(!this.outputType.equals("classlabel")) {
            printHeaderCommandline();
        }

        for(int x=0; x < classifiedInstances.size(); x++) {
            Instances instancesNow = classifiedInstances.get(x);
            if(stringHeader != null) {
                System.out.println(stringHeader.get(x));
            }
            for (int i = 0; i < instancesNow.numInstances(); i++) {
                if (this.outputType.equals("classlabel")) {
                    System.out.println((i + 1) + ": " + instancesNow.get(i).toString(5));
                } else {
                    System.out.println(instancesNow.get(i).toString());
                }
            }
        }
    }

    /**
     * Prints the outcome of a single instances object to the commandline without an arff header above it.
     * with ot without identifying header above the data.
     * @param classifiedInstances the instances which were classified (single instances object), including the predicted classlabel
     * @param stringHeader header to identify the instances by in the output
     */
    void printOutputCommandline(Instances classifiedInstances, String stringHeader) {
        if(stringHeader != null) {
            System.out.println(stringHeader);
        }

        for (int i = 0; i < classifiedInstances.numInstances(); i++) {
            if (this.outputType.equals("classlabel")) {
                System.out.println((i + 1) + ": " + classifiedInstances.get(i).toString(5));
            } else {
                System.out.println(classifiedInstances.get(i).toString());
            }
        }
    }

    /**
     * Prints the arff header associated with the data of this model to the commandline
     */
    private void printHeaderCommandline() {
        try (BufferedReader reader = Files.newBufferedReader(Paths.get("testdata/header.txt"),
                Charset.defaultCharset())) {
            String lineFromFile;
            while ((lineFromFile = reader.readLine()) != null) {
                System.out.println(lineFromFile);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
